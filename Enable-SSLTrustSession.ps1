﻿# Get the script directory to execute via relative pathes:
$scriptDir = Split-Path -Parent -Path $MyInvocation.MyCommand.Path

# Import the SSL C# class definition to execute:
if (-not ([System.Management.Automation.PSTypeName]'IgnoreSsl').Type)
{
    Add-Type -Path "$scriptDir\is.cs"
}

# TRY to set ServicePointManager.ServerCertificateValidationCallback:
try 
{
    # Write before status:
    Write-Host "Enabling SSL Trust Validation..."

    # Execute the C# function -> sets [ServicePointManager] Cert validation:
    [IgnoreSsl]::sv()

    # Write after status:
    Write-Host "SSL Trust Validation has been enabled."
}
catch
{
    Write-Host $_.Exception.Message
}